/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * @author Dan French
 */
public class Scene {
    
    private int height;
    private int width;
    private int distance;
    private String[][] terrain;
    private String[][] object;
    
    //Scene constructor
    public Scene (int height, int width){
        this.height = height;
        this.width = width;
        terrain = new String[height][width];
        object = new String[height][width];
    }
    
    public int getHeight(){
        return height;
    }
    
    public int getWidth(){
        return width;
    }
    
    public void addTerrain(String terrain, int row, int column) {
        this.terrain[row][column] = terrain;
    }

    public String getTerrain(int row, int column) {
        return terrain[row][column];
    }
    
    public void storeObject(String object, int row, int column){
        this.object[row][column] = object;
    }

    public String getObject(int row, int column){
        return object[row][column];
    }
    
    public void calcDistance(int row1, int column1, int row2, int column2){
        int rowDistance;
        int columnDistance;
        if (row1 < row2){
            rowDistance = row2 - row1;
        }
        else {
            rowDistance = row1 - row2;
        }
        if (column1 < column2){
            columnDistance = column2 - column1;
        }
        else {
            columnDistance = column1 - column2;
        }
        distance = rowDistance + columnDistance;
    }
    
    public int getDistance(){
        return distance;
    }
    
    /** Check if a given cell is empty */
    public boolean isEmpty(int row, int column) {
        if (getTerrain(row,column)==null){
            return true;
        }
        else {
            return false;
        }
    }

    /** Count the cells which are not empty */
    public int countItems(int row, int column, int maxDistance) {
       int count = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (!isEmpty(i, j)) {
                    calcDistance(row,column,i,j);
                    if (getDistance() < maxDistance) {
                        count += 1;
                    }
                }
            }
        }
        return count;
    }
}
