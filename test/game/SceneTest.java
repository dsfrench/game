/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dan
 */
public class SceneTest {
    
    //Test of getHeight method, of class Scene.
    @Test
    public void testGetHeight() {
        Scene scene = new Scene(0, 0);
        assertEquals(0, scene.getHeight());
        Scene scene2 = new Scene(5, 10);
        assertEquals(5, scene2.getHeight());
    }
    
    //Test of getWidth method, of class Scene.
    @Test
    public void testGetWidth() {
        Scene scene = new Scene(0, 0);
        assertEquals(0, scene.getWidth());
        Scene scene2 = new Scene(5, 10);
        assertEquals(10, scene2.getWidth());
    }
    
    //Test of addTerrain method, of class Scene.
    @Test
    public void testAddTerrain() {
        Scene scene = new Scene (5, 3);
        scene.addTerrain("G", 2, 1);
        scene.addTerrain("T", 4, 2);
        assertEquals("G", scene.getTerrain(2, 1));
        assertEquals("T", scene.getTerrain(4, 2));
    }
    
    //Test of storeObject method, of class Scene.
    @Test
    public void testStoreObject() {
        Scene scene = new Scene (5, 3);
        scene.storeObject("S", 2, 1);
        scene.storeObject("A", 4, 2);
        assertEquals("S", scene.getObject(2, 1));
        assertEquals("A", scene.getObject(4, 2));
    }
    
    //Test of calcDistance method, of class Scene.
    @Test
    public void testCalcDistance() {
        Scene scene = new Scene (5, 3);
        scene.calcDistance(0, 1, 0, 2);
        assertEquals(1, scene.getDistance());
        scene.calcDistance(0, 1, 2, 1);
        assertEquals(2, scene.getDistance()); 
        scene.calcDistance(2, 4, 0, 1);
        assertEquals(5, scene.getDistance());
    }
    
    //Test of isEmpty method, of class Scene.
    @Test
    public void testIsEmpty() {
        Scene scene = new Scene (5, 3);
        scene.isEmpty(1, 2);
        assertEquals(true, scene.isEmpty(1, 2));
        scene.storeObject("S", 2, 1);
        assertEquals(false, scene.isEmpty(2, 1));
    }
    
    //Test of countItems method, of class Scene.
    @Test
    public void testCountItems() {
        Scene scene = new Scene (5, 3);
        scene.storeObject("S", 2, 1);
        scene.storeObject("A", 4, 2);
        //assertEquals("S", scene.getObject(2, 1));
        //assertEquals("A", scene.getObject(4, 2));
        assertEquals(2, scene.countItems(1, 1, 5));
    }
}
